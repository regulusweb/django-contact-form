from unittest import skipIf

import django
from django.conf import settings
from django.core import mail
from django.test import TestCase, RequestFactory, override_settings

from django.contrib.sites.models import Site

from contact_form.forms import ContactForm


class ContactFormTests(TestCase):
    def test_request_required(self):
        """
        Can't instantiate without an HttpRequest.

        """
        self.assertRaises(TypeError, ContactForm)

    def test_valid_data_required(self):
        """
        Can't try to build the message dict unless data is valid.

        """
        request = RequestFactory().request()
        data = {'name': 'Test',
                'body': 'Test message'}
        form = ContactForm(request=request, data=data)
        self.assertFalse(form.is_valid())

    @override_settings(DEFAULT_EMAIL_RECIPIENT_LIST=['admin@example.com'])
    def test_send(self):
        """
        Valid form can and does in fact send email.

        """
        request = RequestFactory().request()
        data = {'name': 'Test',
                'email': 'test@example.com',
                'body': 'Test message'}
        form = ContactForm(request=request, data=data)
        self.assertTrue(form.is_valid())

        form.save()
        self.assertEqual(1, len(mail.outbox))

        message = mail.outbox[0]

        self.assertEqual(settings.DEFAULT_EMAIL_RECIPIENT_LIST,
                         message.recipients())

        self.assertTrue(data['body'] in message.body)
        from_email = form.from_email_template.format(name=data['name'],
                        email=settings.DEFAULT_FROM_EMAIL)
        self.assertEqual(from_email,
                         message.from_email)

        self.assertEqual(message.extra_headers["Reply-To"],
                         data["email"])

    @skipIf(django.VERSION >= (1, 5, 0), "_meta.installed cannot be modified anymore")
    def test_no_sites(self):
        """
        Sites integration works with or without installed
        contrib.sites.

        """
        old_installed = Site._meta.installed
        Site._meta.installed = False

        request = RequestFactory().request()
        data = {'name': 'Test',
                'email': 'test@example.com',
                'body': 'Test message'}
        form = ContactForm(request=request, data=data)
        self.assertTrue(form.is_valid())

        form.save()
        self.assertEqual(1, len(mail.outbox))

        Site._meta.installed = old_installed
