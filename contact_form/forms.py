"""
A base contact form for allowing users to send email messages through
a web interface, and a subclass demonstrating useful functionality.

"""

from __future__ import unicode_literals
from django import forms
from django.conf import settings
from django.core.mail import EmailMessage
from django.template import loader
from django.template import RequestContext
from django.contrib.sites.requests import RequestSite
from django.contrib.sites.models import Site


class ContactForm(forms.Form):
    """
    The base contact form class from which all contact form classes
    should inherit.

    If you don't need any custom functionality, you can simply use
    this form to provide basic contact functionality; it will collect
    name, email address and message.

    The ``ContactForm`` view included in this application knows how to
    work with this form and can handle many types of subclasses as
    well (see below for a discussion of the important points), so in
    many cases it will be all that you need. If you'd like to use this
    form or a subclass of it from one of your own views, just do the
    following:

    1. When you instantiate the form, pass the current ``HttpRequest``
       object to the constructor as the keyword argument ``request``;
       this is used internally by the base implementation, and also
       made available so that subclasses can add functionality which
       relies on inspecting the request.

    2. To send the message, call the form's ``save`` method, which
       accepts the keyword argument ``fail_silently`` and defaults it
       to ``False``. This argument is passed directly to
       ``send_mail``, and allows you to suppress or raise exceptions
       as needed for debugging. The ``save`` method has no return
       value.

    Other than that, treat it like any other form; validity checks and
    validated data are handled normally, through the ``is_valid``
    method and the ``cleaned_data`` dictionary.


    Base implementation
    -------------------

    Under the hood, this form uses a somewhat abstracted interface in
    order to make it easier to subclass and add functionality. There
    are several important attributes subclasses may want to look at
    overriding, all of which will work (in the base implementation) as
    either plain attributes or as callable methods:

    * ``from_email`` -- used to get the address to use in the
      ``From:`` header of the message. The base implementation returns
      the value of the ``DEFAULT_FROM_EMAIL`` setting.

    * ``from_email_template`` -- used to generate the full text used for
      the ``From:`` header of the message. The base implementation returns
      the value of ``DEFAULT_FROM_EMAIL_TEMPLATE`` setting if set, otherwise
      a default value is used.

    * ``message`` -- used to get the message body as a string. The
      base implementation renders a template using the form's
      ``cleaned_data`` dictionary as context.

    * ``recipient_list`` -- used to generate the list of recipients
      for the message. The base implementation returns the value of
      the ``DEFAULT_EMAIL_RECIPIENT_LIST`` setting or if that doesn't
      exist, it builds a list from the email
      addresses specified in the ``MANAGERS`` setting.

    * ``subject`` -- used to generate the subject line for the
      message. The base implementation returns the string 'Message
      sent through the web site', with the name of the current
      ``Site`` prepended.

    * ``template_name`` -- used by the base ``message`` method to
      determine which template to use for rendering the
      message. Default is ``contact_form/contact_form.txt``.

    Particularly important is the ``message`` attribute, with its base
    implementation as a method which renders a template; because it
    passes ``cleaned_data`` as the template context, any additional
    fields added by a subclass will automatically be available in the
    template. This means that many useful subclasses can get by with
    just adding a few fields and possibly overriding
    ``template_name``.

    Much useful functionality can be achieved in subclasses without
    having to override much of the above; adding additional validation
    methods works the same as any other form, and typically only a few
    items -- ``recipient_list`` and ``subject_line``, for example,
    need to be overridden to achieve customized behavior.


    Other notes for subclassing
    ---------------------------

    Subclasses which want to inspect the current ``HttpRequest`` to
    add functionality can access it via the attribute ``request``; the
    base ``message`` takes advantage of this to use ``RequestContext``
    when rendering its template.

    Subclasses which override ``__init__`` need to accept ``*args``
    and ``**kwargs``, and pass them via ``super`` in order to ensure
    proper behavior.

    Overriding ``save`` is relatively safe, though remember that code
    which uses your form will expect ``save`` to accept the
    ``fail_silently`` keyword argument. In the base implementation,
    that argument defaults to ``False``, on the assumption that it's
    far better to notice errors than to silently not send mail from
    the contact form.

    """
    def __init__(self, data=None, files=None, request=None, *args, **kwargs):
        if request is None:
            raise TypeError("Keyword argument 'request' must be supplied")
        super(ContactForm, self).__init__(data=data, files=files, *args, **kwargs)
        self.request = request

    name = forms.CharField(max_length=100,
                           label=u'Your name')
    email = forms.EmailField(max_length=200,
                             label=u'Your email address')
    body = forms.CharField(widget=forms.Textarea,
                              label=u'Your message')

    from_email = settings.DEFAULT_FROM_EMAIL

    from_email_template = getattr(settings, "DEFAULT_FROM_EMAIL_TEMPLATE", "{name} <{email}>")

    subject_template_name = "contact_form/contact_form_subject.txt"

    template_name = 'contact_form/contact_form.txt'

    @property
    def recipient_list(self):
        # This is a property method because we don't want to get the setting
        # at the time of importing the file as that makes it harder to test
        # the email's recipient_list with a different setting.
        return getattr(settings, "DEFAULT_EMAIL_RECIPIENT_LIST",
                       [mail_tuple[1] for mail_tuple in settings.MANAGERS])

    def message(self):
        """
        Render the body of the message to a string.

        """
        if callable(self.template_name):
            template_name = self.template_name()
        else:
            template_name = self.template_name
        return loader.render_to_string(template_name,
                                       self.get_context())

    def subject(self):
        """
        Render the subject of the message to a string.

        """
        subject = loader.render_to_string(self.subject_template_name,
                                          self.get_context())
        return ''.join(subject.splitlines())

    def get_context(self):
        """
        Return the context used to render the templates for the email
        subject and body.

        By default, this context includes:

        * All of the validated values in the form, as variables of the
          same names as their fields.

        * The current ``Site`` object, as the variable ``site``.

        * The current ``Request`` object, as the variable ``request``.

        """
        if not self.is_valid():
            raise ValueError("Cannot generate Context from invalid contact form")
        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(self.request)

        context = {
          'request': self.request,
          'site': site

        }
        context.update(dict(self.cleaned_data))
        return context

    def save(self, fail_silently=False):
        """
        Build and send the email message.

        """
        # Using the user submitted email address will cause SPF checks
        # on the sending server's domain/ip-address to fail.
        # We use a predefined `from_email` which is expected not to
        # have the above issue.
        # The client's email is used in the 'Reply-To' email header.

        from_email = self.from_email_template.format(name=self.cleaned_data['name'],
                        email=self.from_email)
        headers = {
            'Reply-To': self.cleaned_data['email']
        }

        # Create and send the email
        EmailMessage(self.subject(), self.message(), from_email,
                     self.recipient_list, headers=headers).send(fail_silently=fail_silently)
